
return MakePillarBuilder("deco_wood", "deco_wood_beam", "deco_wood_cornerbeam"),
		MakePillarBuilder("deco_millinery", "deco_millinery_beam", "deco_millinery_cornerbeam"),
		MakePillarBuilder("deco_round", "deco_round_beam", "deco_round_cornerbeam"),
		MakePillarBuilder("deco_marble", "deco_marble_beam", "deco_marble_cornerbeam")
