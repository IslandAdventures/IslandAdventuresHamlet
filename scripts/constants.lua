_G.NUM_RELICS = 5

_G.INTERIORFACING = {
    WALL = 1,
    FLOOR = 2,
}

_G.SPECIAL_FILTER_TABS = {
    RENO_TABS = "reno",
}

_G.SPECIAL_CRAFTING_FILTERS = {
    RENO_TABS = {
        DOORS 		  = { str = "Doors",            icon = "reno_doors_plate.tex" },
        HOME_KITS     = { str = "Home Kits",        icon = "reno_tab_homekits.tex" },
        CHAIRS        = { str = "Chairs",           icon = "reno_tab_chairs.tex" },
        SHELVES       = { str = "Shelves",          icon = "reno_tab_shelves.tex" },
        RUGS          = { str = "Rugs",             icon = "reno_tab_rugs.tex" },
        LAMPS         = { str = "Lamps",            icon = "reno_tab_lamps.tex" },
        PLANT_HOLDERS = { str = "Plantholders",     icon = "reno_tab_plantholders.tex" },
        TABLES        = { str = "Tables",           icon = "reno_tab_tables.tex" },
        ORNAMENTS     = { str = "Ornaments",        icon = "reno_tab_ornaments.tex" },
        WINDOWS       = { str = "Windows",          icon = "reno_tab_windows.tex" },
        COLUMNS       = { str = "Columns",          icon = "reno_tab_columns.tex" },
        FLOORING      = { str = "Floors",           icon = "reno_tab_floors.tex" },
        WALLPAPER     = { str = "Wallpaper",        icon = "reno_tab_wallpaper.tex" },
        HANGING_LAMPS = { str = "Hanging Lamps",    icon = "reno_tab_hanginglamps.tex" }
    },
}

BATS = 
{
	EMPTY = "empty",
	CAVE = "cave",
	ATTACK = "attack",
	CAVE_NUM = 6,
}