


local TileManager = require("tilemanager")
local NoiseTileFunctions = require("noisetilefunctions")

local TileRanges =
{
    LAND = "LAND",
    NOISE = "NOISE",
    OCEAN = "OCEAN",
    IMPASSABLE = "IMPASSABLE",
}

--asgerrr: fixing an obnoxious issue where the mod crashes in the world creation menu if it has already been loaded before (for some reason tiles persist between exiting out of that menu and going back in)
if not GLOBAL.rawget(GLOBAL, "HAMLET_TILES_HAVE_BEEN_DEFINED") then

AddTile(
    "DEEPRAINFOREST",
    TileRanges.LAND,
    {ground_name = "Deep Rainforest"},
    {
        name = "jungle_deep",
        noise_texture = "levels/textures/Ground_noise_jungle_deep.tex",
        runsound = "run_woods",
        walksound = "walk_woods",
        snowsound = "run_snow",
        mudsound = "run_mud"
    },
    {name = "map_edge", noise_texture = "levels/textures/mini_noise_jungle_deep.tex"}
)

AddTile(
    "FOUNDATION",
    TileRanges.LAND,
    {ground_name = "Deep Rainforest"},
    {
        name = "blocky",
        noise_texture = "levels/textures/noise_ruinsbrick_scaled.tex",
        runsound = "run_slate",
        walksound = "walk_slate",
        snowsound = "run_ice",
        mudsound = "run_mud"
    },
    {name = "map_edge", noise_texture = "levels/textures/mini_fanstone_noise.tex"}
)

AddTile(
    "COBBLEROAD",
    TileRanges.LAND,
    {ground_name = "Deep Rainforest"},
    {
        name = "stoneroad",
        noise_texture = "levels/textures/Ground_noise_cobbleroad.tex",
        runsound = "run_rock",
        walksound = "walk_rock",
        snowsound = "run_ice",
        mudsound = "run_mud"
    },
    {name = "map_edge", noise_texture = "levels/textures/mini_brickroad_noise.tex"}
)

AddTile(
    "LAWN",
    TileRanges.LAND,
    {ground_name = "Deep Rainforest"},
    {
        name = "pebble",
        noise_texture = "levels/textures/ground_noise_checkeredlawn.tex",
        runsound = "run_grass",
        walksound = "walk_grass",
        snowsound = "run_snow",
        mudsound = "run_mud"
    },
    {name = "map_edge", noise_texture = "levels/textures/mini_grasslawn_noise.tex"}
)

AddTile(
    "GASJUNGLE",
    TileRanges.LAND,
    {ground_name = "Deep Rainforest"},
    {
        name = "jungle_deep",
        noise_texture = "levels/textures/ground_noise_gas.tex",
        runsound = "run_moss",
        walksound = "walk_moss",
        snowsound = "run_snow",
        mudsound = "run_mud"
    },
    {name = "map_edge", noise_texture = "levels/textures/mini_gasbiome_noise.tex"}
)

AddTile(
    "RAINFOREST",
    TileRanges.LAND,
    {ground_name = "Deep Rainforest"},
    {
        name = "rain_forest",
        noise_texture = "levels/textures/Ground_noise_rainforest.tex",
        runsound = "run_woods",
        walksound = "walk_woods",
        snowsound = "run_snow",
        mudsound = "run_mud"
    },
    {name = "map_edge", noise_texture = "levels/textures/mini_noise_rainforest.tex"}
)

AddTile(
    "INTERIOR",
    TileRanges.LAND,
    {ground_name = "Interior"},
    {
        name = "blocky",
        noise_texture = "levels/textures/interior.tex",
        runsound = "run_dirt",
        walksound = "walk_dirt",
        snowsound = "run_snow",
        mudsound = "run_mud"
    }
)

AddTile(
    "FIELDS",
    TileRanges.LAND,
    {ground_name = "Deep Rainforest"},
    {
        name = "jungle",
        noise_texture = "levels/textures/noise_farmland.tex",
        runsound = "run_woods",
        walksound = "walk_woods",
        snowsound = "run_snow",
        mudsound = "run_mud"
    },
    {name = "map_edge", noise_texture = "levels/textures/mini_noise_farmland.tex"}
)

AddTile(
    "SUBURB",
    TileRanges.LAND,
    {ground_name = "Deep Rainforest"},
    {
        name = "deciduous",
        noise_texture = "levels/textures/noise_mossy_blossom.tex",
        runsound = "run_dirt",
        walksound = "walk_dirt",
        snowsound = "run_ice",
        mudsound = "run_mud"
    },
    {name = "map_edge", noise_texture = "levels/textures/mini_noise_mossy_blossom.tex"}
)

AddTile(
    "PIGRUINS",
    TileRanges.LAND,
    {ground_name = "Deep Rainforest"},
    {
        name = "blocky",
        noise_texture = "levels/textures/interiors/ground_ruins_slab.tex",
        runsound = "run_dirt",
        walksound = "walk_dirt",
        snowsound = "run_ice",
        mudsound = "run_mud"
    },
    {name = "map_edge", noise_texture = "levels/textures/map_interior/mini_ruins_slab.tex"}
)

AddTile(
    "PIGRUINS_NOCANOPY",
    TileRanges.LAND,
    {ground_name = "Deep Rainforest"},
    {
        name = "blocky",
        noise_texture = "levels/textures/interiors/ground_ruins_slab.tex",
        runsound = "run_dirt",
        walksound = "walk_dirt",
        snowsound = "run_ice",
        mudsound = "run_mud"
    },
    {name = "map_edge", noise_texture = "levels/textures/map_interior/mini_ruins_slab.tex"}
)

AddTile(
    "BATTLEGROUND",
    TileRanges.LAND,
    {ground_name = "Deep Rainforest"},
    {
        name = "jungle_deep",
        noise_texture = "levels/textures/Ground_battlegrounds.tex",
        runsound = "run_woods",
        walksound = "walk_woods",
        snowsound = "run_snow",
        mudsound = "run_mud"
    },
    {name = "map_edge", noise_texture = "levels/textures/mini_battlegrounds_noise.tex"}
)

AddTile(
    "PLAINS",
    TileRanges.LAND,
    {ground_name = "Deep Rainforest"},
    {
        name = "jungle",
        noise_texture = "levels/textures/Ground_plains.tex",
        runsound = "run_tallgrass",
        walksound = "walk_tallgrass",
        snowsound = "run_snow",
        mudsound = "run_mud"
    },
    {name = "map_edge", noise_texture = "levels/textures/mini_plains_noise.tex"}
)

AddTile(
    "PAINTED",
    TileRanges.LAND,
    {ground_name = "Deep Rainforest"},
    {
        name = "swamp",
        noise_texture = "levels/textures/Ground_bog.tex",
        runsound = "run_sand",
        walksound = "walk_sand",
        snowsound = "run_snow",
        mudsound = "run_sand"
    },
    {name = "map_edge", noise_texture = "levels/textures/mini_bog_noise.tex"}
)

AddTile(
    "LILYPOND",
    TileRanges.LAND,
    {ground_name = "Deep Rainforest"},
    {
        name = "water_medium",
        noise_texture = "levels/textures/Ground_lilypond2.tex",
        runsound = "run_marsh",
        walksound = "walk_marsh",
        snowsound = "run_snow",
        mudsound = "run_mud"
    },
    {name = "map_edge", noise_texture = "levels/textures/mini_lilypond_noise.tex"}
)

AddTile(
    "DEEPRAINFOREST_NOCANOPY",
    TileRanges.LAND,
    {ground_name = "Deep Rainforest"},
    {
        name = "jungle_deep",
        noise_texture = "levels/textures/Ground_noise_jungle_deep.tex",
        runsound = "run_woods",
        walksound = "walk_woods",
        snowsound = "run_snow",
        mudsound = "run_mud"
    },
    {name = "map_edge", noise_texture = "levels/textures/mini_noise_jungle_deep.tex"}
)

AddTile(
    "BATTLEGROUND_RAINFOREST_NOISE",
    TileRanges.NOISE,
    {}
)
NoiseTileFunctions[WORLD_TILES.BATTLEGROUND_RAINFOREST_NOISE] = function(noise)
    if noise < 0.5 then
        return WORLD_TILES.DIRT
    end
    return WORLD_TILES.RAINFOREST
end

GLOBAL.rawset(GLOBAL, "HAMLET_TILES_HAVE_BEEN_DEFINED", true)
end