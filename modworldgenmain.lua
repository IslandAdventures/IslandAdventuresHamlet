modimport("scripts/tiledefs")
modimport("scripts/map/worldgen_patches")
modimport("scripts/map/ia_lockandkey")
modimport("scripts/map/new_layouts")

modimport("scripts/map/startlocations/porkland")
modimport("scripts/map/tasks/porkland")
modimport("scripts/map/tasksets/porklandset")
modimport("scripts/map/levels/porkland")


